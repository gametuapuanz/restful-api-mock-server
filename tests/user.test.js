const dbHandler = require('./db-handler')
const User = require('../models/User')

// Connect to a new in-memory database before running any tests.
beforeAll(async () => {
  await dbHandler.connect()
})

// Clear all test data after every test.
afterEach(async () => {
  await dbHandler.clearDatabase()
})

// Complete user example.
const userComplete1 = {
  name: 'O-live',
  gender: 'M'
}
// Complete user example.
const userComplete2 = {
  name: 'O-live',
  gender: 'F'
}

// Error name user example.
const userErrorNameEmpty = {
  name: '',
  gender: 'M'
}

// Error name user example.
const userErrorGenderInvalid = {
  name: 'Game',
  gender: 'A'
}

// Errot user example.
const userErrorName2 = {
  name: 'Ra',
  gender: 'M'
}

// User test suite.
describe('User ', () => {
  it('สามารถเพิ่ม user ได้', async () => {
    let error = null
    try {
      const user = new User(userComplete1)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่ม user ได้', async () => {
    let error = null
    try {
      const user = new User(userComplete2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
})

// User test suite.
describe('User ', () => {
  it('ไม่สามารถเพิ่ม user ได้ เพาะชื่อเป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorNameEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม user ได้ เพาะชื่อมี 2 ตัว', async () => {
    let error = null
    try {
      const user = new User(userErrorName2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม user ได้ เพาะ gender ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new User(userErrorGenderInvalid)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม user ได้ เพราะ ชื่อซ้ำกัน', async () => {
    let error = null
    try {
      const user1 = new User(userComplete1)
      await user1.save()
      const user2 = new User(userComplete1)
      await user2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
