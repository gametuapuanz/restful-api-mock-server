const User = require('../models/User')

const userController = {
  userList: [
    { id: 1, name: 'rawich', gender: 'M' },
    { id: 2, name: 'olive', gender: 'M' }
  ],
  lastId: 3,
  // addUser (user) {
  //   user.id = this.lastId++
  //   this.userList.push(user)
  //   return user
  // },
  // updateUser (user) {
  //   const index = this.userList.findIndex(item => item.id === user.id)
  //   this.userList.splice(index, 1, user)
  //   return user
  // },
  // deleteUser (id) {
  //   const index = this.userList.findIndex(item => item.id === parseInt(id))
  //   this.userList.splice(index, 1)
  //   return { id: id }
  // },
  // getUsers() {
  //   return [...this.userList]
  // },
  // getUser (id) {
  //   const user = this.userList.find(item => item.id === parseInt(id))
  //   return user
  // },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    //  res.json(userController.getUser(id))
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addUser (req, res, next) {
    const payload = req.body
    //res.json(userController.addUser(payload))
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    //res.json(userController.updateUser(payload))
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    //res.json(userController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
