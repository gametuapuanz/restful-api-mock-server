jest:

    > npm install --save-dev jest
    > npx jest --runInBand ./tests
#install

    > npm install

#run

    > npm run start

#URL:http://localhost:3000/

- Git global setup
- git config --global user.name ""
- git config --global user.email ""
-
- Create a new repository
- git clone https://gitlab.com/gametuapuanz/restful-api-mock-server.git
- cd restful-api-mock-server
- touch README.md
- git add README.md
- git commit -m "add README"
- git push -u origin master
-
- Push an existing folder
- cd existing_folder
- git init
- git remote add origin https://gitlab.com/gametuapuanz/restful-api-mock-server.git
- git add .
- git commit -m "Initial commit"
- git push -u origin master
-
- Push an existing Git repository
- cd existing_repo
- git remote rename origin old-origin
- git remote add origin https://gitlab.com/gametuapuanz/restful-api-mock-server.git
- git push -u origin --all
- git push -u origin --tags
-
