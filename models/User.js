const mongoose = require('mongoose')
mongoose.connect('mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('Connect!')
})

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    enum: ['M', 'F'],
    required: true
  }
})

module.exports = mongoose.model('User', userSchema)
